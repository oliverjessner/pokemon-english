# pokemon-english 
I got inspired by [sindresorhus](https://github.com/sindresorhus/pokemon)
    
## Install

```
$ npm install --save pokemon-english
```

## Usage

```js
const pokemon = require('pokemon-english');
console.log(pokemon[0]); // Bulbasaur
```

## All available Languages
- [English](https://github.com/oliver-j/pokemon-english)
- [German](https://github.com/oliver-j/pokemon-german)
- [French](https://github.com/oliver-j/pokemon-french)
- [Korean](https://github.com/oliver-j/pokemon-korean)
- [Japanese](https://github.com/oliver-j/pokemon-japanese)

## License

MIT © [Oliver-j](https://twitter.com/oliverj_net)